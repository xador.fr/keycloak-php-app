# étape de récupération des dépendances
FROM composer:2 as composer
WORKDIR /app
COPY composer.json ./
RUN composer install

# création container final
FROM php:8.0-apache
COPY --from=composer /app/vendor /vendor/
RUN a2enmod rewrite
RUN apt-get update \
	&& apt-get install -y libpq-dev libcurl3-dev && docker-php-ext-install pdo pdo_mysql curl \
	&& rm -rf /var/lib/apt/lists/*

COPY src /var/www/html
