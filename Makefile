REGISTRY_ID=php-keycloak
PORT?=8080
TAG=latest

## DEV LOCAL

build:
	docker build -t $(REGISTRY_ID):$(TAG) .

dev: build
	docker run --rm -it -p $(PORT):80 -v `pwd`/src:/var/www/html $(REGISTRY_ID):$(TAG)
