<?php
require_once '/vendor/autoload.php';

function initAuth($provider) {
	if (! isset($_GET['code'])) {
		/* get authorization code */
		$authUrl = $provider->getAuthorizationUrl();
		$_SESSION['oauth2state'] = $provider->getState();
		header("Location: $authUrl");
		exit;
	} 
	
	/* check given state against previously stored one to mitigate CSRF attack */
	if (empty($_GET['state']) || $_GET['state'] !== $_SESSION['oauth2state']) {
		unset($_SESSION['oauth2state']);
		exit('Invalid state, make sure HTTP sessions are enabled.');
	}
	
	/* get access token (authorization code grant) */
	try {
		return $provider->getAccessToken('authorization_code', [ 'code' => $_GET['code'] ]);
	} catch (Exception $e) {
		exit('Failed to get access token: '.$e->getMessage());
	}
}

$provider = new Stevenmaguire\OAuth2\Client\Provider\Keycloak([
	'authServerUrl'         => 'https://sso.meteo-routes.com/',
	'realm'                 => 'meteo-routes',
	'clientId'              => 'meteo-omnium',
	'clientSecret'          => '...',
	'redirectUri'           => 'http://localhost:8080',
]);

session_start();

$token = @$_SESSION['token'];
if($token != null) {
	try {
		if($token->getExpires() - time() < 60) {
			$token = $provider->getAccessToken('refresh_token', [ 'refresh_token' => $token->getRefreshToken() ]);
		}
	} catch(Throwable $err) {
		unset($_SESSION['token']);
		$token = initAuth($provider);
	}
} else {
	$token = initAuth($provider);
}

$_SESSION['token'] = $token;
$account = $provider->getResourceOwner($token);

// echo "Hello {$account->getName()} (sub {$account->getId()})";

/* use sub in order to map account with app user : $account->getId() */
